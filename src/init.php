<?php
/**
 * Blocks Initializer
 *
 * Enqueue CSS/JS of all the blocks.
 *
 * @since   1.0.0
 * @package BS
 */

// Exit if accessed directly.
if (!defined('ABSPATH')) {
	exit;
}

$block = 'block-bs-content-simple';

// Hook server side rendering into render callback
register_block_type('bonseo/' . $block,
	array(
		'attributes' => array(
			'title' => array(
				'type' => 'string',
			),
			'content' => array(
				'type' => 'string',
			),
			'heading' => array(
				'type' => 'string',
			),
			'className' => array(
				'type' => 'string',
			),
		),
		'render_callback' => 'render_bs_content_simple',
	)
);

function bs_content_simple_editor_assets()
{
	wp_enqueue_script(
		'bs_content_simple-block-js', // Handle.
		plugins_url('/dist/blocks.build.js', dirname(__FILE__)), // Block.build.js: We register the block here. Built with Webpack.
		array('wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor'), // Dependencies, defined above.
		// filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.build.js' ), // Version: File modification time.
		true // Enqueue the script in the footer.
	);
}

function render_bs_content_simple_header($header, $title) {
	return '<' . $header . ' class="a-text a-text--center a-text--l a-text--bold a-text--brand a-pad--y">' . $title . '</' . $header . '>';
}

function render_bs_content_simple($attributes)
{
	$class = isset($attributes['className']) ? ' ' . $attributes['className'] : '';
	$title = isset($attributes['title']) ? $attributes["title"] : '';
	$content = isset($attributes['content']) ? $attributes["content"] : '';
	$headSize= isset($attributes['heading']) ? $attributes['heading'] : 'h1';
	$heading = isset($attributes['title']) ? render_bs_content_simple_header($headSize, $title) : '';
	return '
	<div class="og-content-plain a-pad-20 ' . $class . '">
    	' . $heading . '
    	' . $content . '
    </div>';
}

// Hook: Editor assets.
add_action('enqueue_block_editor_assets', 'bs_content_simple_editor_assets');
